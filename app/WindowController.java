package app;

import app.expressions.Expression;
import app.expressions.ExpressionParser;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

public class WindowController {
	private IStorage storage;
	
	@FXML private TextField txtInput;
	@FXML private TextField txtOutput;
	@FXML private TreeView<String> treeOutput;
	
	public WindowController() {
		storage = new ColdStorage("prevcalc.txt");
	}
	
	@FXML
	private void initialize() {
		String prevCalc = storage.loadState();
		txtInput.setText(prevCalc);
		refreshCalculation(prevCalc);
	}
	
	@FXML
	private void txtInput_onAction() {
		refreshCalculation(txtInput.getText());
		storage.saveState(txtInput.getText());
	}
	
	private void refreshCalculation(String input) {
		try {
			Expression exprTree = ExpressionParser.parseExpression(input);
			
			refreshTree(exprTree);
			refreshOutput(exprTree);
		}
		catch (IllegalArgumentException ex) {
			setError(ex.getMessage());
		}
	}
	
	private void refreshOutput(Expression expr) {
		double output = expr.evaluate();
		txtOutput.setText(Double.toString(output));
	}
	
	private void refreshTree(Expression expr) {
		TreeItem<String> rootItem = getNodeFromExpression(expr);
        treeOutput.setRoot(rootItem);
	}
	
	private void setError(String msg) {
		txtOutput.clear();
		
		TreeItem<String> rootItem = new TreeItem<String> (msg);
        treeOutput.setRoot(rootItem);
	}
	
	private TreeItem<String> getNodeFromExpression(Expression expr) {
		TreeItem<String> node = new TreeItem<String>(expr.toString());
		Expression[] exprChildren = expr.getSubexpressions();
		if (exprChildren != null) {
			for (Expression subexpr : exprChildren) {
				TreeItem<String> childNode = getNodeFromExpression(subexpr);
				node.getChildren().add(childNode);
			}
			node.setExpanded(true);
		}
		return node;
	}
}
