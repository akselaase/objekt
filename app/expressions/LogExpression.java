package app.expressions;

public class LogExpression extends Expression {
	private Expression core;
	private Expression base;
	
	public LogExpression(Expression core, Expression base) {
		this.core = core;
		this.base = base;
	}

	@Override
	public double evaluate() {
		return Math.log(core.evaluate()) / Math.log(base.evaluate());
	}

	@Override
	public Expression[] getSubexpressions() {
		return new Expression[] {core, base};
	}
	
	@Override
	public String toString() {
		return "log(x, base=" + base.evalString() + "): " + evaluate();
	}
}
