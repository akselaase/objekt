package app.expressions;

public abstract class Expression {
	public abstract double evaluate();
	public abstract Expression[] getSubexpressions();
	
	public String evalString() {
		return Double.toString(evaluate());
	}
}
