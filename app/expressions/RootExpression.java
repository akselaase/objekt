package app.expressions;

public class RootExpression extends Expression {
	private Expression core;
	private Expression base;
	
	public RootExpression(Expression core, Expression base) {
		this.core = core;
		this.base = base;
	}

	@Override
	public double evaluate() {
		return Math.pow(core.evaluate(), 1 / base.evaluate());
	}

	@Override
	public Expression[] getSubexpressions() {
		return new Expression[] {core, base};
	}
	
	@Override
	public String toString() {
		return "root(x, base=" + base.evalString() + "): " + evaluate();
	}
}
