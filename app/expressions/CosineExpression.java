package app.expressions;

public class CosineExpression extends Expression {
	private Expression core;
	
	public CosineExpression(Expression core) {
		this.core = core;
	}

	@Override
	public double evaluate() {
		return Math.cos(core.evaluate());
	}

	@Override
	public Expression[] getSubexpressions() {
		return new Expression[] {core};
	}
	
	@Override
	public String toString() {
		return "cos(x): " + evaluate();
	}
}
