package app.expressions;

public class SymbolicExpression extends ConstantExpression {
	private String symbol;
	
	public SymbolicExpression(double value, String symbol) {
		super(value);
		this.symbol = symbol;
	}

	@Override
	public String toString() {
		return "Constant: " + symbol;
	}
	
	@Override 
	public String evalString() {
		return symbol;
	}
}