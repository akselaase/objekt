package app.expressions;

public class ConstantExpression extends Expression {
	private double value;
	
	public ConstantExpression(double value) {
		this.value = value;
	}
	
	@Override
	public double evaluate() {
		return value;
	}
	
	@Override
	public Expression[] getSubexpressions() {
		return null;
	}
	
	@Override
	public String toString() {
		return "Constant: " + value;
	}
}
