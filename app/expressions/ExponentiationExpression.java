package app.expressions;

public class ExponentiationExpression extends Expression {
	private Expression base, power;
	
	public ExponentiationExpression(Expression base, Expression right) {
		this.base = base;
		this.power = right;
	}

	@Override
	public double evaluate() {
		return Math.pow(base.evaluate(), power.evaluate());
	}

	@Override
	public Expression[] getSubexpressions() {
		return new Expression[] {base, power};
	}
	
	@Override
	public String toString() {
		return "pow(x, pow=" + power.evalString() + "): " + evaluate();
	}
}
