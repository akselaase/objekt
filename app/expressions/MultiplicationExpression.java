package app.expressions;

public class MultiplicationExpression extends Expression {
	private Expression left, right;
	
	public MultiplicationExpression(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public double evaluate() {
		return left.evaluate() * right.evaluate();
	}

	@Override
	public Expression[] getSubexpressions() {
		return new Expression[] {left, right};
	}
	
	@Override
	public String toString() {
		return "Multiply: " + evaluate();
	}
}
