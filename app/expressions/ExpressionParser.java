package app.expressions;

import java.util.NoSuchElementException;

public final class ExpressionParser {
	
	public static Expression parseExpression(String input) {
		ExpressionBuilder builder = new ExpressionBuilder();
		
		for (String c : input.toLowerCase().split(" ")) {
			try {
				switch (c) {
				case "+":
					builder.pushAddition(); break;
				case "-":
					builder.pushSubtraction(); break;
				case "*":
					builder.pushMultiplication(); break;
				case "/":
					builder.pushDivision(); break;
				case "^":
					builder.pushExponentiation(); break;
				case "sqrt":
					builder.pushSqrt(); break;
				case "root":
					builder.pushRoot(); break;
				case "sin":
					builder.pushSine(); break;
				case "cos":
					builder.pushCosine(); break;
				case "ln": 
					builder.pushLn(); break;
				case "log": 
					builder.pushLog(); break;
				case "e":
					builder.pushSymbol(Math.E, "e"); break;
				case "pi":
					builder.pushSymbol(Math.PI, "pi"); break;
				default:
					double val = Double.parseDouble(c);
					builder.pushConstant(val);
				}
			}
			catch (NoSuchElementException ex) {
				throw new IllegalArgumentException("Not enough elements on the stack to perform operation.");
			}
			catch (NumberFormatException ex) {
				throw new IllegalArgumentException("Unknown operation/value '" + c + "'.");
			}
		}
		
		return builder.toExpression();
	}
}
