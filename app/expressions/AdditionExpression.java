package app.expressions;

public class AdditionExpression extends Expression {
	private Expression left, right;
	
	public AdditionExpression(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public double evaluate() {
		return left.evaluate() + right.evaluate();
	}

	@Override
	public Expression[] getSubexpressions() {
		return new Expression[] {left, right};
	}
	
	@Override
	public String toString() {
		return "Add: " + evaluate();
	}
}
