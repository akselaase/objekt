package app.expressions;

import java.util.ArrayDeque;
import java.util.Deque;

public class ExpressionBuilder {
	private Deque<Expression> exprStack;
	
	public ExpressionBuilder() {
		exprStack = new ArrayDeque<Expression>();
	}
	
	public Expression toExpression() {
		return exprStack.peek();
	}
	
	public void pushConstant(double value) {
		exprStack.push(new ConstantExpression(value));
	}
	
	public void pushSymbol(double value, String name) {
		exprStack.push(new SymbolicExpression(value, name));
	}
	
	public void pushAddition() {
		Expression right = exprStack.pop();
		Expression left = exprStack.pop();
		exprStack.push(new AdditionExpression(left, right));
	}
	
	public void pushSubtraction() {
		Expression right = exprStack.pop();
		Expression left = exprStack.pop();
		exprStack.push(new SubtractionExpression(left, right));
	}
	
	public void pushMultiplication() {
		Expression right = exprStack.pop();
		Expression left = exprStack.pop();
		exprStack.push(new MultiplicationExpression(left, right));
	}
	
	public void pushDivision() {
		Expression right = exprStack.pop();
		Expression left = exprStack.pop();
		exprStack.push(new DivisionExpression(left, right));
	}
	
	public void pushExponentiation() {
		Expression power = exprStack.pop();
		Expression base = exprStack.pop();
		exprStack.push(new ExponentiationExpression(base, power));
	}

	public void pushSqrt() {
		Expression core = exprStack.pop();
		exprStack.push(new RootExpression(core, new ConstantExpression(2)));
	}
	
	public void pushRoot() {
		Expression base = exprStack.pop();
		Expression core = exprStack.pop();
		exprStack.push(new RootExpression(core, base));
	}
	
	public void pushSine() {
		Expression core = exprStack.pop();
		exprStack.push(new SineExpression(core));
	}
	
	public void pushCosine() {
		Expression core = exprStack.pop();
		exprStack.push(new CosineExpression(core));
	}
	
	public void pushLn() {
		Expression core = exprStack.pop();
		exprStack.push(new LogExpression(core, new SymbolicExpression(Math.E, "e")));
	}
	
	public void pushLog() {
		Expression base = exprStack.pop();
		Expression core = exprStack.pop();
		exprStack.push(new LogExpression(core, base));
	}
}
