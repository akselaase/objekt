package app.expressions;

public class SineExpression extends Expression {
	private Expression core;
	
	public SineExpression(Expression core) {
		this.core = core;
	}

	@Override
	public double evaluate() {
		return Math.sin(core.evaluate());
	}

	@Override
	public Expression[] getSubexpressions() {
		return new Expression[] {core};
	}
	
	@Override
	public String toString() {
		return "sin(x): " + evaluate();
	}
}
