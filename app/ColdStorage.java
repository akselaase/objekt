package app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ColdStorage implements IStorage {
	private String fileName;
	
	public ColdStorage(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public void saveState(String state) {
		try {
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(state);
            fileWriter.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error writing file '" 
                + fileName + "'");
        }
	}

	@Override
	public String loadState() {
		try {
	        FileReader fileReader = new FileReader(fileName);
	        BufferedReader bufferedReader = new BufferedReader(fileReader);
	        
	        String line = bufferedReader.readLine();
	        bufferedReader.close();      
	        if (line != null)
	        	return line;
	    }
	    catch(FileNotFoundException ex) {
	        System.out.println(
	            "Unable to open file '" + 
	            fileName + "'");                
	    }
	    catch(IOException ex) {
	        System.out.println(
	            "Error reading file '" 
	            + fileName + "'");
	    }
		return "";
	}

}
