package app;

public interface IStorage {
	public void saveState(String state);
	public String loadState();
}
