package encapsulation;

public class Name {
	private String first;
	private String last;

	public Name(String fullName) throws IllegalArgumentException {
		this(fullName, " ");
	}

	public Name(String fullName, String separator) throws IllegalArgumentException {
		String[] parts = fullName.split(separator);
		if (parts.length != 2)
			throw new IllegalArgumentException("Name was not two parts: " + fullName);
		if (parts[0].length() < 2 | parts[1].length() < 2)
			throw new IllegalArgumentException("Names were not long enough");
		
		for (int i = 0; i < parts[0].length(); i++)
			if (!Character.isLetter(parts[0].charAt(i)))
				throw new IllegalArgumentException("Found non-letter");
		for (int i = 0; i < parts[1].length(); i++)
			if (!Character.isLetter(parts[1].charAt(i)))
				throw new IllegalArgumentException("Found non-letter");
				
		this.first = parts[0];
		this.last = parts[1];
	}

	public String getFirst() {
		return first;
	}

	public String getLast() {
		return last;
	}
	
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other == this) return true;
		if (!(other instanceof Name)) return false;
		return toString().equals(other.toString());
	}

	@Override
	public String toString() {
		return toString(" ");
	}

	public String toString(String separator) {
		return first + separator + last;
	}
}
