package encapsulation;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

public class RPNCalc {
	private Deque<Double> stack;
	
	public RPNCalc() {
		stack = new ArrayDeque<Double>();
	} 
	
	public void push(double value) {
		stack.push(value);
	}
	
	public double pop() {
		try {
			return stack.pop();
		}
		catch (NoSuchElementException ex) {
			return Double.NaN;
		}
	}
	
	public double peek(int index) {
		try {
			return stack.toArray(new Double[0])[index];
		}
		catch (IndexOutOfBoundsException ex) {
			return Double.NaN;
		}
	}
	
	public int getSize() {
		return stack.size();
	}
	
	void performOperation(char c) {
		double v1 = pop();
		double v2 = pop();
		
		switch (c) {
		case '+':
			push(v1 + v2);
			break;
		case '-':
			push(v2 - v1);
			break;
		case '*':
			push(v1 * v2);
			break;
		case '/':
			push(v2 / v1);
			break;
		case '~':
			push(v1);
			push(v2);
			break;
		case 'p':
			push(v2);
			push(v1);
			push(Math.PI);
			break;
		case '|':
			push(v2);
			push(Math.abs(v1));
			break;
		}
	}
}
