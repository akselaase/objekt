package encapsulation;

public class Account {
	private double balance;
	private double interestRate;

	public Account(double balance, double interest) {
		if (balance < 0 || interest < 0)
			throw new IllegalArgumentException();
		this.balance = balance;
		this.interestRate = interest;
	}

	public void deposit(double value) {
		if (value < 0)
			throw new IllegalArgumentException();
		this.balance += value;
	}

	public void withdraw(double value) {
		if (value < 0)
			throw new IllegalArgumentException();
		if (value > balance)
			throw new IllegalArgumentException();
		this.balance -= value;
	}

	public void addInterest() {
		this.balance += this.balance * this.interestRate / 100.0;
	}

	public double getBalance() {
		return this.balance;
	}

	public double getInterestRate() {
		return this.interestRate;
	}

	public void setInterestRate(double value) {
		if (value < 0)
			throw new IllegalArgumentException();
		this.interestRate = value;
	}
}
