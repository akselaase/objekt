package encapsulation;

import java.util.Date;

public class Person {
	private Name name;
	private EmailAddress email;
	private Date birthday;
	private char gender;
	
	public Person() {
		
	}

	public String getName() {
		if (name == null)
			return "";
		return name.toString();
	}

	public void setName(String name) {
		this.name = new Name(name);
	}

	public String getEmail() {
		if (email == null)
			return "";
		return email.toString();
	}

	public void setEmail(String email) throws IllegalArgumentException {
		EmailAddress new_addr = new EmailAddress(email);
		if (!name.getFirst().toLowerCase().equals(new_addr.getUser().getFirst()) | 
				!name.getLast().toLowerCase().equals(new_addr.getUser().getLast())	)
			throw new IllegalArgumentException();
		this.email = new_addr;
	}

	public Date getBirthday	() {
		return birthday;
	}

	public void setBirthday	(Date date) {
		if (date.after(new Date()))
			throw new IllegalArgumentException();
		this.birthday = date;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		if (gender != 'M' & gender != 'F' & gender != '\0')
			throw new IllegalArgumentException();
		this.gender = gender;
	}
}
