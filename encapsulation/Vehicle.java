
package encapsulation;

public class Vehicle {
	private char carType;
	private char fuelType;
	private String regNr;
	
	public Vehicle(char carType, char fuelType, String regNr) {
		if (carType != 'M' && carType != 'C')
			throw new IllegalArgumentException();		
		if (fuelType != 'H' && fuelType != 'E' && fuelType != 'D' && fuelType != 'G') 
			throw new IllegalArgumentException();
		if (fuelType == 'H' && carType != 'C')
			throw new IllegalArgumentException();
				
		this.carType = carType;
		this.fuelType = fuelType;
		
		setRegistrationNumber(regNr);
	}
	
	public char getFuelType() {
		return fuelType;
	}
	
	public char getVehicleType() {
		return carType;
	}
	
	public String getRegistrationNumber() {
		return regNr;
	}
	
	public void setRegistrationNumber(String regNr) {
		if (fuelType == 'H' && !regNr.substring(0, 2).equals("HY"))
			throw new IllegalArgumentException();
		if (fuelType != 'H' && regNr.substring(0, 2).equals("HY"))
			throw new IllegalArgumentException();
		
		if (fuelType == 'E' && 
			!(regNr.substring(0, 2).equals("EL") | regNr.substring(0, 2).equals("EK")))
			throw new IllegalArgumentException();
		if (fuelType != 'E' && 
			(regNr.substring(0, 2).equals("EL") | regNr.substring(0, 2).equals("EK")))
			throw new IllegalArgumentException();
		
		if (carType == 'M' && regNr.length() != 6)
			throw new IllegalArgumentException();
		if (carType == 'C' && regNr.length() != 7)
			throw new IllegalArgumentException();

		for (int i = 0; i < 2; i++) {
			int cv = (int)regNr.charAt(i);
			if (cv < (int)'A' || cv > (int)'Z')
				throw new IllegalArgumentException();
		}
		for (int i = 2; i < regNr.length(); i++) {
			char ch = regNr.charAt(i);
			if (!Character.isDigit(ch))
				throw new IllegalArgumentException();
		}
		
		this.regNr = regNr;
	}
	
}
