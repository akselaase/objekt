package interfaces;

import java.util.Iterator;
import java.util.function.BinaryOperator;

public class BinaryComputingIterator implements Iterator<Double> {
	private Iterator<Double> iter1;
	private Iterator<Double> iter2;
	private Double default1;
	private Double default2;
	private BinaryOperator<Double> operator;
	
	public BinaryComputingIterator(Iterator<Double> iter1, Iterator<Double> iter2, BinaryOperator<Double> op) {
		this.iter1 = iter1;
		this.iter2 = iter2;
		this.operator = op;
		this.default1 = null;
		this.default2 = null;
	}
	
	public BinaryComputingIterator(Iterator<Double> iter1, Iterator<Double> iter2,
			Double default1, Double default2, BinaryOperator<Double> op) {
		this.iter1 = iter1;
		this.iter2 = iter2;
		this.operator = op;
		this.default1 = default1;
		this.default2 = default2;
	}

	@Override
	public boolean hasNext() {
		if (!iter1.hasNext() && !iter2.hasNext())
			return false;
		if (!iter1.hasNext() && default1 == null)
			return false;
		if (!iter2.hasNext() && default2 == null)
			return false;
		return true;
	}

	@Override
	public Double next() {
		Double v1, v2;
		v1 = iter1.hasNext() ? iter1.next() : default1;
		v2 = iter2.hasNext() ? iter2.next() : default2;
		return operator.apply(v1, v2);
	}

}
