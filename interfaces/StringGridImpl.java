package interfaces;

import java.util.ArrayList;
import java.util.Iterator;

public class StringGridImpl implements StringGrid {
	private ArrayList<String> cells;
	private int rows;
	private int cols;

	public StringGridImpl(int rows, int columns) {
		cells = new ArrayList<String>(rows * columns);
		this.rows = rows;
		this.cols = columns;
		for (int i = 0; i < rows * columns; i++) {
			cells.add("");
		}
	}

	@Override
	public int getRowCount() {
		return rows;
	}

	@Override
	public int getColumnCount() {
		return cols;
	}

	@Override
	public String getElement(int row, int column) {
		return cells.get(row * cols + column);
	}

	@Override
	public void setElement(int row, int column, String element) {
		cells.set(row * cols + column, element);
	}

	@Override
	public Iterator<String> iterator() {
		return new StringGridIterator(this, true);
	}

}
