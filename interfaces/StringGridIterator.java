package interfaces;

import java.util.Iterator;

public class StringGridIterator implements Iterator<String> {
	private StringGrid grid;
	private boolean rowMajor;
	private int x, y;
	
	public StringGridIterator(StringGrid grid, boolean rowMajor) {
		this.grid = grid;
		this.rowMajor = rowMajor;
		x=0;
		y=0;
	}

	@Override
	public boolean hasNext() {
		return x != grid.getColumnCount() && y != grid.getRowCount() ;
	}

	@Override
	public String next() {
		String res = grid.getElement(y,  x);
		if (rowMajor) {
			x = (x + 1) % grid.getColumnCount();
			if (x == 0)
				y += 1;
		}
		else{
			y = (y + 1) % grid.getRowCount();
			if (y == 0)
				x += 1;
		}
		return res;
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}	
}
