package stateandbehavior;

public class UpOrDownCounter {
	private int curr, end;
	private int direction;

	public UpOrDownCounter(int start, int end) {
		if (start == end)
			throw new IllegalArgumentException();
		this.curr = start;
		this.end = end;
		this.direction = Integer.signum(end - start);
	}

	public int getCounter() {
		return curr;
	}
	
	public boolean count() {
		curr += direction;
		direction = Integer.signum(end - curr);
		return direction != 0;
	}
}
