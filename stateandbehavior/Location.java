package stateandbehavior;

public class Location {
	private int x = 0, y = 0;

	public void up() {
		y -= 1;
	}

	public void down() {
		y += 1;
	}

	public void left() {
		x -= 1;
	}

	public void right() {
		x += 1;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}
	
	@Override
	public String toString() {
		return Integer.toString(x) + ", " + Integer.toString(y);
	}

	public static void main(String[] argv) {
		Location l = new Location();
		System.out.println(l);
		l.up();
		System.out.println(l);
		l.right();
		System.out.println(l);
		l.down();
		System.out.println(l);
		l.left();
		System.out.println(l);
	}
}
