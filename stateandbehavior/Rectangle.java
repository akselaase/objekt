package stateandbehavior;

public class Rectangle {
	private int minX, minY, maxX, maxY;
	private boolean empty = true;
	
	public int getMinX() {return minX;}
	public int getMinY() {return minY;}
	public int getMaxX() {return maxX;}
	public int getMaxY() {return maxY;}
	
	public int getWidth() {return isEmpty() ? 0 : maxX - minX + 1;}
	public int getHeight() {return isEmpty() ? 0 : maxY - minY + 1;}
	
	public boolean isEmpty() {return empty; }

	public boolean contains(int x, int y) {
		return !isEmpty() & (minX <= x & x <= maxX) & (minY <= y & y <= maxY);
	}
	
	public boolean contains(Rectangle rect) {
		return !rect.isEmpty() & contains(rect.minX, rect.minY) & contains(rect.maxX, rect.maxY); 
	}
	
	public boolean add(int x, int y) {
		boolean modified = false;
		if (empty)
		{
			minX = maxX = x;
			minY = maxY = y;
			empty = false;
			modified = true;
		}
		if (x < minX) {
			minX = x;
			modified = true;
		}
		else if (x > maxX) {
			maxX = x;
			modified = true;
		}
		if (y < minY) {
			minY = y;
			modified = true;
		}
		else if(y > maxY) {
			maxY = y;
			modified = true;
		}			
		return modified;
	}
	
	public boolean add(Rectangle rect) {
		if (rect.isEmpty())
			return false;
		return add(rect.minX, rect.minY) | add(rect.maxX, rect.maxY); 
	}
	
	public Rectangle union(Rectangle rect) {
		Rectangle r = new Rectangle();
		r.add(this);
		r.add(rect);
		return r;
	}
}
