package stateandbehavior;

public class Digit {
	private int base;
	private int value;

	public Digit(int base) {
		this.base = base;
		value = 0;
	}

	public int getValue() {
		return value;
	}

	public int getBase() {
		return base;
	}

	public boolean increment() {
		value = (value + 1) % base;
		return value == 0;
	}

	public String toString() {
		if (value < 10)
			return Integer.toString(value);
		else
			return Character.toString((char)(value + 65 - 10));
	}

	public static void main(String[] argv) {
		Digit d = new Digit(3);
		System.out.println(d.increment());
		System.out.println(d.increment());
		System.out.println(d.increment());
		System.out.println(d.increment());
		System.out.println(d.increment());
		System.out.println(d.increment());
	}
}
