package stateandbehavior;

public class LineEditor {
	private String text = "";
	private int insertionIndex;

	public void left() {
		insertionIndex = Math.max(0, insertionIndex - 1);
	}

	public void right() {
		insertionIndex = Math.min(text.length(), insertionIndex + 1);
	}
	
	public void insertString(String s) {
		text = text.substring(0,  insertionIndex) + s + text.substring(insertionIndex);
		insertionIndex += s.length();
	}
	
	public void deleteLeft() {
		if (insertionIndex == 0)
			return;
		text = text.substring(0, insertionIndex - 1) + text.substring(insertionIndex);
		insertionIndex -= 1;
	}
	
	public void deleteRight() {
		if (insertionIndex == text.length()) 
			return;
		text = text.substring(0, insertionIndex) + text.substring(insertionIndex + 1);
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String s) {
		text = s;
		insertionIndex = Math.min(insertionIndex, s.length());
	}
	
	public int getInsertionIndex() {
		return insertionIndex;	
	}

	public void setInsertionIndex(int value) {
		insertionIndex = value;
	}
	
	public String toString() {
		return text.substring(0, insertionIndex) + "|" + text.substring(insertionIndex);
	}
}
