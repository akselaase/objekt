package stateandbehavior;

public class StopWatch {
	private boolean started;
	private boolean stopped;
	private int totalTicks;
	private int currTicks;
	private int lapStart;
	private int lastLapTicks;
	
	public StopWatch() {
		started = stopped = false;
		totalTicks = 0;
		currTicks = -1;
		lapStart = 0;
		lastLapTicks = -1;
	}

	public boolean isStarted() {
		return started;
	}

	public boolean isStopped() {
		return stopped;
	}

	public int getTicks() {
		return totalTicks;
	}

	public int getTime() {
		return currTicks;
	}

	public int getLapTime() {
		return currTicks - lapStart;
	}

	public int getLastLapTime() {
		return lastLapTicks;
	}

	public void tick(int ticks) {
		this.totalTicks += ticks;
		if (isStarted() && !isStopped()) {
			this.currTicks += ticks;
		}
	}

	public void start() {
		started = true;
		currTicks = 0;
		lastLapTicks = 0;
	}

	public void stop() {
		stopped = true;
		lap();
	}

	public void lap() {
		lastLapTicks = currTicks - lapStart;
		lapStart = currTicks;
	}
}
