package stateandbehavior;

public class Account {
	private double balance;
	private double interestRate;
	
	public void deposit(double value) {
		if (value > 0) {
			this.balance += value;
		}
	}
	
	public void addInterest() {
		this.balance += this.balance * this.interestRate / 100.0;
	}
	
	public double getBalance() {
		return this.balance;
	}
	
	public double getInterestRate () {
		return this.interestRate;
	}
	
	public void setInterestRate(double value) {
		this.interestRate = value;
	}
	
	@Override
	public String toString() {
		return Double.toString(balance) + ", " + Double.toString(interestRate);
	}
	
	public static void main(String[] argv) {
		Account l = new Account();
		System.out.println(l);
		l.setInterestRate(10);
		System.out.println(l);
		l.deposit(100);
		System.out.println(l);
		l.addInterest();
		System.out.println(l);
	}
}
