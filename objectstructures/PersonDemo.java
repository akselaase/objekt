package objectstructures;

public final class PersonDemo {

	public static void main(String[] args) {
		Person mormor = new Person("Dikkeda", 'F');
		Person morfar = new Person("Torbjørn", 'M');
		Person mor = new Person("Lise", 'F');
		Person farmor = new Person("Britta", 'F');
		Person farfar = new Person("Gunnar", 'M');
		Person far = new Person("Nils", 'M');
		Person meg = new Person("Aksel", 'M');
		mor.setMother(mormor);
		mor.setFather(morfar);
		far.setMother(farmor);
		far.setFather(farfar);
		meg.setMother(mor);
		meg.setFather(far);
	}

}
