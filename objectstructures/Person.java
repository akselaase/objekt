package objectstructures;

import java.util.ArrayList;

public class Person {
	private String name;
	private char gender;
	private Person mother;
	private Person father;
	private ArrayList<Person> children;
	
	public Person(String name, char gender) {
		if (gender != 'M' && gender != 'F')
			throw new IllegalArgumentException("Gender must be 'M' or 'F'");
		
		this.name = name;
		this.gender = gender;
		this.children = new ArrayList<Person>();
	}
	
	public Person getMother() {
		return mother;
	}
	
	public Person getFather() {
		return father;
	}
	
	public String getName() {
		return name;
	}

	public char getGender() {
		return gender;
	}
	
	public int getChildCount() {
		return children.size();
	}
	
	public Person getChild(int index) {
		if (index < 0 || index >= getChildCount())
			throw new IllegalArgumentException();
		return children.get(index);
	}
	
	public void addChild(Person child) {
		Person.replaceRelationship(child.getParent(this.gender), this, child);
	}
	
	public void removeChild(Person child) {
		Person.dissolveRelationship(this, child);
	}
	
	public void setMother(Person mother) {
		if (mother != null && mother.getGender() != 'F')
			throw new IllegalArgumentException();
		Person.replaceRelationship(this.mother, mother, this);
	}
	
	public void setFather(Person father) {
		if (father != null && father.getGender() != 'M')
			throw new IllegalArgumentException();
		Person.replaceRelationship(this.father, father, this);
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	
	private Person getParent(char gender) {
		if (gender == 'F')
			return mother;
		else
			return father;
	}
	
	private void setParent(char gender, Person value) {
		if (gender == 'F')
			mother = value;
		else
			father = value;
	}
	
	private static void replaceRelationship(Person prevParent, Person newParent, Person child) {
		char gender;
		if (prevParent != null)
			gender = prevParent.gender;
		else if (newParent != null)
			gender = newParent.gender;
		else
			return;
		Person.dissolveRelationship(prevParent, child);
		Person.establishRelationship(gender, newParent, child);
	}
	
	private static void establishRelationship(char parentGender, Person parent, Person child) {
		if (child == parent)
			throw new IllegalArgumentException();
		
		child.setParent(parentGender, parent);
		if (parent != null)
			parent.children.add(child);
	}
	
	private static void dissolveRelationship(Person parent, Person child) {
		if (parent == null)
			return;
		if (child == null)
			return;
		child.setParent(parent.gender, null);
		parent.children.remove(child);
	}
}
